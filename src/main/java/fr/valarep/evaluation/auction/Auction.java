package fr.valarep.evaluation.auction;

import java.util.Date;

public class Auction {
    private long id;
    private Date startDate;
    private Date endDate;
    private Double startPrice;
    private String product;

    public long getId() {
        return id;
    }

    public Auction(long id, Date startDate, Date endDate, Double startPrice, String product) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startPrice = startPrice;
        this.product = product;
    }

    public Auction() {
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Double getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Double startPrice) {
        this.startPrice = startPrice;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}
