package fr.valarep.evaluation.auction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Repository
public class AuctionRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public AuctionRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void save(Auction auction) {
        String sql = "insert into auction(start_date, end_date, start_price, product) "
                + "values (:startDate, :endDate, :startPrice, :product);";

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("startDate", auction.getStartDate());
        parameters.put("endDate", auction.getEndDate());
        parameters.put("startPrice", auction.getStartPrice());
        parameters.put("product", auction.getProduct());

        jdbcTemplate.update(sql, parameters);
    }

    public Auction findAuctionById(Long id) {
        Auction auction = new Auction();
        auction.setId(id);
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(auction);
        try {
            return jdbcTemplate.queryForObject("select * from auction where id = :id;", namedParameters, new AuctionMapper());
        } catch (DataAccessException e) {
            System.err.println("no result");
            return null;
        }
    }

    private static final class AuctionMapper implements RowMapper<Auction> {

        public Auction mapRow(ResultSet rs, int rowNum) throws SQLException {
            Auction auction = new Auction();
            auction.setId(rs.getLong("ID"));
            auction.setStartDate(rs.getDate("START_DATE"));
            auction.setEndDate(rs.getDate("END_DATE"));
            auction.setStartPrice(rs.getDouble("START_PRICE"));
            auction.setProduct(rs.getString("PRODUCT"));

            return auction;
        }
    }
}
