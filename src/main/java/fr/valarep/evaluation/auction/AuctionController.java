package fr.valarep.evaluation.auction;

import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/auctions")
public class AuctionController {
    private static Map<Long, Auction> auctions = new HashMap<>();
    private AuctionRepository auctionRepository;

    @Autowired
    public AuctionController(AuctionRepository auctionRepository) {
        this.auctionRepository = auctionRepository;
    }

    @PostMapping
    @ApiOperation(value = "Add a auction", tags = "auctions")
    @ResponseStatus(CREATED)
    public void addAuction(@RequestBody Auction auction) {
        auctionRepository.save(auction);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get  a auction", tags = "auctions")
    public ResponseEntity<Auction> getAuction(@PathVariable long id) {
        Auction auction = auctionRepository.findAuctionById(id);
        if (auction == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return new ResponseEntity<>(CREATED);
    }
}
